<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 03.03.2017
 * Time: 16:19
 */

$meta_array = \XmagChild\BookMetaBox::getMeta(get_the_ID());
if(!empty($meta_array)):
    foreach ($meta_array as $name => $value):
        ?>
        <p><b><?= $name ?> </b><?= $value ?></p>
        <?php
    endforeach;
endif;