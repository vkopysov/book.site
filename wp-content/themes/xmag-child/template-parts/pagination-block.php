<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 03.03.2017
 * Time: 9:27
 */
?>

<div class="pagination">
            <?php
                global $wp_query;

                if(isset($filterFlag)) {
                    \XmagChild\BookPagination::addPagination($wp_query->max_num_pages, $addArgs);
                } else {
                    \XmagChild\BookPagination::addPagination($wp_query->max_num_pages);
                }
            ?>
</div>