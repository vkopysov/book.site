<?php
/**
 * The template for main content (books-page / ajax filter).
 *
 * @package xMag
 * @since xMag 1.0
 */
?>
    <main id="main" class="site-main" role="main">
        <div class="posts-loop">
            <?php
            \XmagChild\BookWPQuery::getWPQuery($args);
            ?>
        </div>
        <?php
            include( get_stylesheet_directory(). '/template-parts/pagination-block.php');
        ?>
    </main><!-- #main -->