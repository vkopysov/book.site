<?php

require_once (__DIR__.'/src/CustomPostType.php');
require_once (__DIR__ . '/src/CustomTaxonomy.php');
require_once (__DIR__.'/src/BookMetaBox.php');
require_once (__DIR__.'/src/ThemeOption.php');
require_once (__DIR__.'/src/BookNavMenu.php');
require_once (__DIR__.'/src/CustomSidebar.php');
require_once (__DIR__.'/src/BookFilterWidget.php');
require_once (__DIR__.'/src/BookAjaxFilterWidget.php');
require_once (__DIR__.'/src/BookPagination.php');
require_once (__DIR__.'/src/BookWPQuery.php');

add_action( 'wp_enqueue_scripts', function() {
    wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );
});


//Registering Book Post Type
$bookArgs = include (__DIR__.'/inc/book-post-type.php');
$bookPostType = new \XmagChild\CustomPostType($bookArgs, 'book');

//Registering Custom Taxonomies for Book Post Type
$bookTaxonomies = [];
$taxArgs = include (__DIR__.'/inc/book-taxonomies.php');
foreach ( $taxArgs as $key => $value) {
    $bookTaxonomies[$key] = new \XmagChild\CustomTaxonomy($value, $key, $bookPostType->getName());
}

//Creating Book Meta Box
$bookMetaBox = new \XmagChild\BookMetaBox();

//Option page with Logo and Copyright settings
$options = new \XmagChild\ThemeOption();


//Adding sidebar which will be visible on book page
$sidebarArgs = include (__DIR__.'/inc/book-sidebar-area.php');
$bookSidebar = new \XmagChild\CustomSidebar($sidebarArgs);

//Creating Book Filter widget
$bookFilterWidget = new \XmagChild\BookFilterWidget();

//Creating Ajax Book Filter
$bookAjaxFilterWidget = new \XmagChild\BookAjaxFilterWidget();
$bookAjaxFilterWidget->setAjax();

