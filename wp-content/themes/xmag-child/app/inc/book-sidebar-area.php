<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 27.02.2017
 * Time: 13:01
 */

return [
    'name'          => __( 'Book Sidebar', 'xmag' ),
    'id'            => 'book-sidebar',
    'before_widget' => '<aside id="%1$s" class="widget %2$s">',
    'after_widget'  => '</aside>',
    'before_title'  => '<h1 class="widget-title">',
    'after_title'   => '</h1>',
];