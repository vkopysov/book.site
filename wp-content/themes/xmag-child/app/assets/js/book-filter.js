;(function( $, window, document, undefined ){

    $( document ).ready( function ($) {

        if (typeof(filterAjax.url) != "undefined" &&
            typeof(filterAjax.nonce) != "undefined" &&
            typeof(filterAjax.bookUrl) != "undefined" ) {

            //ajax data
            var data = {
                action: 'filter_action',
                nonce : filterAjax.nonce
            };

            //widget click event
            $('aside.widget_book_ajax_filter_widget').on('click', "input[type='checkbox']", function () {

                // if  checkboxes are not checked
                data.selected = false;

                //checkbox object
                var selected = {
                    taxonomy: [],
                    term_id: []
                };

                //if checked push to selected
                $("aside.widget_book_ajax_filter_widget input:checked").each(function() {
                    selected.taxonomy.push($(this).attr('tax-name'));
                    selected.term_id.push($(this).val());
                    data.selected = selected;

                });

                //make ajax with pushState
                makeAjax(data, true);
            });

            //html 5 history popstate (backward/forward) event
            $(window).on('popstate', function(e) {

                data.selected = history.state;

                //uncheck checkboxes
                //$("aside.widget_book_ajax_filter_widget input:checkbox:checked").each(function() {
                $("input:checkbox:checked").each(function() {
                    $(this).prop("checked",false);
                });

                if(data.selected != null && data.selected != false ) {

                    //check needed checkboxes
                    data.selected.term_id.forEach(function (item){
                        //$("aside.widget_book_ajax_filter_widget input:checkbox[value="+ item +"]").prop("checked",true);
                        $("input:checkbox[value="+ item +"]").prop("checked",true);
                    });
                } else {
                    data.selected = {};
                }

               // refresh page via ajax
                makeAjax(data, false);
            });

            // ajax
            // pushStateFlag for for enebling history.pushState
            function makeAjax (data, pushStateFlag) {
                $.post(filterAjax.url, data, function (response) {
                    //success
                    if (response.success) {
                        $("#main").replaceWith(response.data);
                        //
                        if (pushStateFlag) {
                            // book page url
                            var url = filterAjax.bookUrl;

                            if (data.selected) {

                                if( filterAjax.permalink_struct != "" ) {
                                    url += '?';
                                } else {
                                    url += '&';
                                }

                                // html5 history url
                                var i = 0;
                                for (i; i < data.selected.taxonomy.length; i++) {
                                    url += data.selected.taxonomy[i] + '[]=' + data.selected.term_id[i];
                                    if ( i != data.selected.taxonomy.length - 1 ) {
                                        url += '&';
                                    }
                                }
                            }

                            history.pushState(data.selected, null, url);
                        }
                        //
                    } else {
                        $("#main").replaceWith('<h1>Ошибка подключения</h1>');
                    }
                });
            }

        } else {
            $("#main").replaceWith('<h1>Ошибка подключения</h1>');
        }
    } );

})( jQuery, window , document );