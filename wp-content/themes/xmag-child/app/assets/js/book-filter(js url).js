;(function( $, window, document, undefined ){

    $( document ).ready( function ($) {

        if (typeof(filterAjax.url) != "undefined" &&
            typeof(filterAjax.nonce) != "undefined" &&
            typeof(filterAjax.bookUrl) != "undefined" ) {

            //checkbox object
            var selected = {
                taxonomy: [],
                term_id: [],
            };

            //ajax data
            var data = {
                action: 'filter_action',
                nonce : filterAjax.nonce,
                selected: selected
            };

            //widget click event
            $('aside.widget_book_ajax_filter_widget').on('click', "input[type='checkbox']", function () {

                // book page url
                var url = filterAjax.bookUrl;

                var selected = {
                    taxonomy: [],
                    term_id: [],
                };

                $("aside.widget_book_ajax_filter_widget input:checked").each(function() {
                    selected.taxonomy.push($(this).attr('tax-name'));
                    selected.term_id.push($(this).val());
                    data.selected = selected;
                });

                // html5 history

                var i = 0;
                for (i; i < data.selected.taxonomy.length; i++) {
                    url += '?' + data.selected.taxonomy[i] + '[]=' + data.selected.term_id[i];
                    if ( i != data.selected.taxonomy.length - 1 ) {
                        url += '&';
                    }
                }

               // history.pushState(null, null, url);
                history.pushState(data.selected, null, url);

                makeAjax(data);

            });

            //html 5 history popstate (backward/forward) event
            $(window).on('popstate', function(e) {
                var params = [];
                console.log(state);
                selected = {
                    taxonomy: [],
                    term_id: [],
                };

                //getting parametres from url for ajax
                window.location.search.substr(1).split("?").forEach(function (item) {
                    var term = [];
                    params = item.split("=");
                    if(params[0] != "") {
                        selected.taxonomy.push(params[0].substr(0, params[0].length - 2 ));

                        term = params[1].split("&");
                        selected.term_id.push(term[0]);

                        data.selected = selected;
                    } else {
                        data.selected = {};
                    }
                    //uncheck checked checkboxes
                    $("aside.widget_book_ajax_filter_widget input:checkbox:checked").each(function() {
                  //  $("input:checkbox:checked").each(function() {
                        $(this).prop("checked",false);
                    });

                    //check needed checkboxes
                    selected.term_id.forEach(function (item){
                        //$("aside.widget_book_ajax_filter_widget input:checkbox[value="+ item +"]").prop("checked",true);
                        $("input:checkbox[value="+ item +"]").prop("checked",true);
                    });


                });
               // refresh page via ajax
                makeAjax(data);
            });

            // ajax
            function makeAjax (data) {
                $.post(filterAjax.url, data, function (response) {
                    //success
                    if (response.success) {
                        $("#main").replaceWith(response.data);
                    } else {
                        $("#main").replaceWith('<h1>Ошибка подключения</h1>');
                    }
                });
            }
        } else {
            $("#main").replaceWith('<h1>Ошибка подключения</h1>');
        }
    } );

})( jQuery, window , document );