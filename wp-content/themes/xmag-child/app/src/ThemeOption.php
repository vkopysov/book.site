<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 12:47
 */

namespace XmagChild;


class ThemeOption
{

    //private $attachmentId;

    public function __construct()
    {
        add_action( 'admin_menu', [ $this, 'add_theme_menu_item' ] );
        add_action("admin_init", [ $this, 'display_theme_panel_fields' ]);

        add_action( 'after_setup_theme', [$this, 'set_custom_logo_size'] );
    }

    public function add_theme_menu_item()
    {
        add_menu_page("Панель настроек", "Дополнительные настройки", "manage_options", "theme-panel", [$this, 'theme_settings_page'], null, 99);
    }

    public function theme_settings_page()
    {
        ?>
        <div class="wrap">
            <form method="post"  enctype=multipart/form-data action="options.php">
                <?php
                settings_fields("section");
                do_settings_sections("theme-options");
                submit_button();
                ?>
            </form>
        </div>
        <?php
    }

    public function display_theme_panel_fields()
    {
        add_settings_section("section", "Настройки копирайта и логотипа", null, "theme-options");

        add_settings_field("logo_c", "Выберите Логотип:", [$this, "logo_display"], "theme-options", "section");
        add_settings_field("copyright", "Введите Копирайт:", [$this, "copyright_display"], "theme-options", "section");
        add_settings_field("book-page", "Страница для добавления подменю и вывода виджета", [$this, "book_page_display"], "theme-options", "section");

        register_setting("section", "logo_c", [$this,  "handle_logo"]);
        register_setting("section", "copyright", [$this,  "handle_copyright"]);
        register_setting("section", "book-page", [$this,  "handle_book_page"]);
    }


    public function logo_display()
    {
        $logo = get_option('logo_c');
        if($logo && $logo != '') {
            echo wp_get_attachment_image($logo, 'custom-logo');
            ?>
            <br>
            <input type="file" name="logo"/>
            <p>
                <input type="checkbox" name="logo_delete"/>
                <label for = "logo_delete"> Убрать логотип </label>
            </p>
            <?php
        } else {
            echo "<p> Лого не выбран </p>";
            ?>
            <br>
            <input type="file" name="logo"/>
            <p>
                <input type="checkbox" name="logo_delete" checked/>
                <label for = "logo_delete"> Убрать логотип </label>
            </p>
            <?php
        }
    }

    public function copyright_display()
    {
        $option = get_option('copyright');
        ?>
        <input type="text" name="copyright" value="<?php echo esc_attr($option); ?> "/>
        <?php
    }

    public function book_page_display()
    {
        $option = get_option('book-page');

        $args = array(
            'sort_order'   => 'ASC',
            'sort_column'  => 'post_title',
            'post_type'    => 'page',
            'post_status'  => 'publish',
            'meta_key'     => '_wp_page_template',
            'meta_value'   => 'page-books.php',
        );

        $pages = get_pages($args);

        if (!empty($pages)):
            ?>
            <select name="book-pages">
                <?php
                foreach ($pages as $page) :
                    ?>
                    <option value="<?php echo $page->ID ; ?>"  <?php echo selected( ($option == $page->ID ) ? 1 : 0 ) ?> ><?php echo $page->post_title ; ?></option>
                    <?php
                endforeach;
                ?>
            </select>
            <?php
        else :
            ?>
            <p>Не найдено страниц с шаблоном "Страница книг" (page-book)</p>
            <p>Необходимо создать страницу и назначить ее шаблон.</p>
            <?php
        endif;
    }

    public function handle_logo()
    {
        if($_FILES['logo']['tmp_name'])
        {
            $mediaHandle = media_handle_upload('logo', 0, [], ["test_form" => FALSE]);
            if (is_int($mediaHandle)) {
               // $this->attachmentId = $mediaHandle;
                return $mediaHandle;
            }
            //return $this->attachmentId;

        } else {
            if($_POST['logo_delete']) {
                return '';
            }
            return get_option('logo_c');
        }
    }

    public function handle_copyright()
    {
        if(isset($_POST['copyright']) && trim($_POST['copyright']) != '' ) {
            return sanitize_text_field($_POST['copyright']);
        } else {
            return '© default Copyright ';
        }
    }

    public function handle_book_page()
    {
        if(isset($_POST['book-pages'])) {
            return intval($_POST['book-pages']);
        }
    }

    public function set_custom_logo_size() {
        add_image_size('custom-logo', 60, 60);
    }
}