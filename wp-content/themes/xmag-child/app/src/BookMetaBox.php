<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 10:33
 */

namespace XmagChild;

class BookMetaBox
{
    CONST POST_TYPE = 'book';

    CONST META_FIELDS = [
            'page_quantity',
            'isbn',
            'release_date'
    ];

    public function __construct()
    {
        add_action('add_meta_boxes', [$this, 'add']);
        add_action("save_post_".self::POST_TYPE, [$this, 'save']);
       // add_action("save_post", [$this, 'save']);
    }

    public function add()
    {
        add_meta_box(
            'book_meta_box',                // Unique ID
            'Дополнительная информация: ',   // Box title
            [$this, 'html'],                // Content callback, must be of type callable
            'book'                          // Post type
        );
    }

    public static function html($post)
    {
        foreach (self::META_FIELDS as $meta_field):
            $meta_value = get_post_meta($post->ID, $meta_field, true);
                ?>
                <p>
                    <label for="<?php echo $meta_field ;?>">  <?php echo self::displayFieldName($meta_field) ;?> </label>
                    <input class="widefat" type="text"  name="<?php echo $meta_field ;?>" value="<?php echo  $meta_value  ; ?>">
                </p>
                <?php
        endforeach;
    }

    public function save($post_id)
    {

        foreach (self::META_FIELDS as $meta_field) {

            switch ($meta_field) {
                case 'page_quantity':
                    if (intval($_POST[$meta_field])) {
                        update_post_meta($post_id, $meta_field, $_POST[$meta_field]);
                    }
                    break;
                default:
                        update_post_meta($post_id, $meta_field, sanitize_text_field($_POST[$meta_field]));
                    break;
            }
        }

    }

    public static function getMeta($id)
    {
        $meta_arr = [];
        foreach (self::META_FIELDS as $meta_field) {
            $post_meta = get_post_meta($id, $meta_field, true);
            if(!empty($post_meta)) {
                $meta_arr[self::displayFieldName($meta_field)] = $post_meta;
            }
        }
        return $meta_arr;
    }

    public static function displayFieldName($meta_field)
    {
        switch ($meta_field) {
            case 'page_quantity':
                return 'Количество страниц: ';
            case 'isbn':
                return 'ISBN: ';
            case 'release_date':
                return 'Дата релиза: ';
        }
    }
}
