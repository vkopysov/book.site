<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 27.02.2017
 * Time: 13:00
 */

namespace XmagChild;


class CustomSidebar
{
    public function __construct($args)
    {
        $this->args = $args;

        add_action( 'widgets_init', [$this, 'register_custom_sidebar'] );
    }

    public function register_custom_sidebar()
    {
        register_sidebar($this->args);
    }
}