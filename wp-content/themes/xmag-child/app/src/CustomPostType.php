<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 9:39
 */

namespace XmagChild;


class CustomPostType
{
    private $name;

    private $args;

    public function __construct($args, $name = 'book')
    {
        $this->name = $name;
        $this->args = $args;

        add_action( 'init', [ $this, 'register_custom_post_type' ]);
    }

    public function register_custom_post_type()
    {
        register_post_type($this->name, $this->args);
    }

    public function getName()
    {
        return $this->name;
    }
}
