<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 22.02.2017
 * Time: 11:59
 */

namespace XmagChild;


class CustomTaxonomy
{
    private $name;

    private $postTypeName;

    private $args;

    public function __construct($args, $name, $postTypeName)
    {
        $this->name = $name;
        $this->args = $args;
        $this->postTypeName = $postTypeName;

        add_action('init', [$this, 'register_custom_taxonomy']);
    }

    public function register_custom_taxonomy()
    {
        register_taxonomy($this->name, [$this->postTypeName], $this->args);
    }
}
