<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 28.02.2017
 * Time: 17:52
 */

namespace XmagChild;


class BookWPQuery
{
    public static function getWPQuery($args)
    {
        $args['posts_per_page'] = 10;

        $wpQuery = new \WP_Query($args);

        global $wp_query;
        $wp_query =  $wpQuery;

        if ($wpQuery->have_posts()) {

            while ($wpQuery->have_posts()) {

                $wpQuery->the_post();

                get_template_part('template-parts/content', 'bookspage');

            }
        } else {
            get_template_part('template-parts/content', 'none');
        }

    }

    /**
     * Forming taxonomu condition for WP_QUERY
     *
     * @param $queryArr filtered $_GET parameters
     * @return array
     */
    public static function formTaxQuery($queryArr)
    {
        $permalinkStructure = get_option('permalink_structure');
        if ($permalinkStructure == "" ) {
            if(isset($queryArr['page_id'])){
                unset($queryArr['page_id']);
            }
            if(isset($queryArr['paged']))
                unset($queryArr['paged']);
        }

        $taxQuery = [
            'relation' => 'OR',
        ];

        foreach ($queryArr as $taxName => $termArr){
            if($termArr) {
           // if($termArr) {
                $termIds = [];

                foreach ($termArr as $termId) {
                    if ($termId) {
                        $termIds[] = $termId;
                    }
                }

                $taxQuery[] = [
                    'taxonomy'   => $taxName,
                    'field'      => 'term_id',
                    'terms'      => $termIds
                ];
            }
        }
        return $taxQuery;
    }
}