<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 27.02.2017
 * Time: 9:28
 */

namespace XmagChild;


class BookNavMenu extends \Walker_Nav_Menu
{
    public function __construct()
    {
        add_filter( 'nav_menu_css_class', [ $this, 'has_children_nav_class'], 10, 2 );
    }

    /**
     * Adding class for dropdown icon
     * @param $classes
     * @param $item
     * @return array
     */

    public function has_children_nav_class( $classes, $item ) {

        $taxNames =  get_object_taxonomies('book');

        if ( $item->object_id == get_option('book-page') && !empty($taxNames)) {
            $classes[] = "menu-item-has-children";
        }
        return $classes;
    }

    /**
     * Adding submenus to menu item
     *
     * @param string $output
     * @param \WP_Post $item
     * @param int $depth
     * @param array $args
     */
    public function end_el(&$output, $item, $depth=0, $args=array())
    {
        $bookTaxonomyNames = get_object_taxonomies('book');

        if ( $item->object_id == get_option('book-page') && !empty($bookTaxonomyNames) ) {

            ob_start();
            ?>
            <ul class='sub-menu'>
                <?php

                foreach ($bookTaxonomyNames as $taxName) :
                    $tax = get_taxonomy($taxName);
                    $terms = get_terms($tax->name);
                    if (!empty($terms)):

                    ?>
                    <li id='menu-item-<?php echo esc_attr($tax->name) ; ?>'
                        class='menu-item menu-item-type-post_type menu-item-has-children menu-item-object-page menu-item-<?php echo  esc_attr($tax->name) ; ?>'>
                        <a href='#'>
                            <?php echo esc_html($tax->label); ?>
                        </a>
                        <?php
                            echo '<ul class="sub-menu">';
                            foreach ($terms as $term) :
                            ?>
                                <li id="menu-item-<?php echo esc_attr($term->term_id); ?>" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-<?php echo esc_attr($term->term_id); ?>">
                                    <a href="<?php echo esc_url(get_site_url() .'/'. $tax->name .'/'. $term->slug); ?>"><?php echo esc_html($term->name); ?></a>
                                </li>
                            <?php
                            endforeach;
                            echo '</ul>';
                        ?>
                    </li>
                    <?php
                    endif;
                endforeach;
                ?>
            </ul>
            <?php
            $output .= ob_get_clean();
        }  else {
            parent::end_el( $output, $item, $depth, $args);
        }
    }
}
