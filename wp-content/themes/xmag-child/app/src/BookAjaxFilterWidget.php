<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 27.02.2017
 * Time: 13:36
 */

namespace XmagChild;


class BookAjaxFilterWidget extends \WP_Widget
{
    public function __construct() {
        // actual widget processes
        parent::__construct(
            'book_ajax_filter_widget',  // Base ID
            'Фильтр Книг (ajax)'   // Name
        );
        add_action( 'widgets_init', [$this, 'register_book_ajax_widget']);
    }

    public function register_book_ajax_widget()
    {
        register_widget( __CLASS__ );
    }

    public  function widget($args, $instance)
    {
        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . "<h1> {$instance['title']} </h1>" . $args['after_title'];
        }

        if(isset($instance['taxonomies'])) {
            ?>
            <form method="get" autocomplete="off">
            <?php
            foreach ($instance['taxonomies'] as $taxName => $value) :
                $taxObj = get_taxonomy($taxName);
                if(isset($_GET[$taxName])) {
                    $termIds = filter_var_array($_GET[$taxName], FILTER_VALIDATE_INT);
                } else {
                    $termIds = [ false ];
                }
                ?>

                <h2> <?php echo  esc_html($taxObj->label); ?></h2>
                    <?php
                        $terms = get_terms($taxName);
                        foreach ($terms as $term) :
                            ?>
                                    <input type="checkbox" class="checkbox"
                                           id="<?php echo esc_attr('a-'.$taxName.'-'.$term->term_id) ;?>"
                                           name="<?php echo esc_attr($taxName) ;?>"
                                           tax-name="<?php echo esc_attr($taxName)?>"
                                           value="<?php echo esc_attr($term->term_id) ?>"
                                            <?php
                                            echo checked (in_array($term->term_id, $termIds) ? 1 : 0) ;
                                            ?>
                                    />
                                    <label for="<?php echo esc_attr('a-'.$taxName.'-'.$term->term_id) ?>"><?php _e( esc_html($term->name) ); ?></label><br />
                            <?php
                        endforeach;
                    ?>
                <?php
            endforeach;
            ?>
            <br>
            </form>
            <?php
        }
        echo $args['after_widget'];
    }

    public function form( $instance )
    {
        // outputs the options form in the admin
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = "";
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat"
                   id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>"
                   type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
        $taxonomies = get_object_taxonomies('book');
        foreach ($taxonomies as $taxonomy) :
            $taxObj = get_taxonomy($taxonomy);
            ?>
            <input type="checkbox"
                   class="checkbox"
                   id="<?php echo $this->get_field_id($taxonomy); ?>"
                   name="<?php echo $this->get_field_name($taxonomy); ?>"
                   value="1"

                  <?php echo  checked ((isset($instance['taxonomies'][$taxonomy])) ? $instance['taxonomies'][$taxonomy] : 0); ?>
            />
            <label for="<?php echo $this->get_field_id($taxonomy); ?>"><?php _e( "Фильтрация по \"".$taxObj->label."\"" ); ?></label><br />
            <?php
        endforeach;
    }

    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = [];

        $taxonomies = get_object_taxonomies('book');
        foreach ($taxonomies as $taxonomy) {
            $instance['taxonomies'][$taxonomy] =  !empty( $new_instance[$taxonomy] ) ? 1 : 0;
        }
           $instance['title'] = ( !empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        return $instance;
    }


    public function ajax_callback()
    {
        $nonce = filter_var($_POST['nonce'], FILTER_SANITIZE_STRING) ;

        if ( !wp_verify_nonce( $nonce, 'filterAjax-nonce' ) ) {
            wp_send_json_error();
        }

        $taxonomyNames =    filter_var_array($_POST['selected']['taxonomy'], FILTER_SANITIZE_STRING);
        $termIds =          filter_var_array($_POST['selected']['term_id'], FILTER_VALIDATE_INT);

        $taxCounter = 0;

        $tempArr = [];
        foreach ( $taxonomyNames  as $taxonomyName) {
            $tempArr[$taxonomyName][] = $termIds[$taxCounter];
            $taxCounter++;
        }
        unset($taxCounter);

        $taxQuery = [
            'relation' => 'OR',
        ];

        foreach ($tempArr as $taxName => $termIds) {
            $taxQuery[] = [
                'taxonomy'   => $taxName,
                'field'      => 'term_id',
                'terms'      => $termIds
            ];
        }

        $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;

        $args = [
            'post_type'         => 'book',
            'tax_query'         => $taxQuery,
            'paged'             => $paged,
        ];

        // pagination add_args for paginate_links

        $addArgs = 0;
        if(!empty($tempArr)) {
            $addArgs = $tempArr;
        }
        $filterFlag = true;

        ob_start();
            include_once( get_stylesheet_directory(). '/template-parts/main-block.php');
        $html = ob_get_clean();

        wp_send_json_success($html);
        $_GET['test'] = '123';
    }

    public function setAjax()
    {
        //registering js and setting admin-ajax url for frontend
        add_action( 'wp_enqueue_scripts', [$this, 'ajax_filter_script'] );

        //adding callbacks on ajax
        if( wp_doing_ajax() ){
            add_action('wp_ajax_filter_action', [$this, 'ajax_callback']);
            add_action('wp_ajax_nopriv_filter_action', [$this, 'ajax_callback']);
        }
    }

    public function ajax_filter_script()
    {
        wp_enqueue_script( 'book-filter', get_stylesheet_directory_uri() . '/app/assets/js/book-filter.js', ['jquery'], '1.0.0', true );

        wp_localize_script('book-filter', 'filterAjax',
            [
                'url'               => admin_url('admin-ajax.php'),
                'nonce'             => wp_create_nonce('filterAjax-nonce'),
                'bookUrl'           => get_page_link(get_option('book-page')),
                'permalink_struct'  => get_option('permalink_structure')
            ]
        );
    }
}
