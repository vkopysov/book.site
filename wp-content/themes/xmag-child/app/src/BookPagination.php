<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 01.03.2017
 * Time: 10:20
 */

namespace XmagChild;


class BookPagination
{
    /**
     * @param $total
     * @param int $filterFlag flag to change  pagination links when using pagination from ajax filter
     */
    public static function addPagination($total, $addArgs = 0)
    {
        $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;

        $bookPageUrl =  get_page_link( get_option('book-page') );

        $permalinkStructure = get_option('permalink_structure');

        if ($permalinkStructure != "") {
            $base = $bookPageUrl . "page/%#%/";
        } else {
            //$base = $bookPageUrl . "&page=%#%";
            $base = $bookPageUrl . "&paged=%#%";
        }

        echo paginate_links(array(
            'base' => $base,
            'format' => '?paged=%#%',
            'current' => max(1, $paged),
            'total' => $total,
            'add_args' => $addArgs
        ));
    }
}