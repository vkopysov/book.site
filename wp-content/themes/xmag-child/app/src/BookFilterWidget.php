<?php
/**
 * Created by PhpStorm.
 * User: kopysov
 * Date: 27.02.2017
 * Time: 13:36
 */

namespace XmagChild;


class BookFilterWidget extends \WP_Widget
{
    public function __construct() {
        // actual widget processes
        parent::__construct(
            'book_filter_widget',  // Base ID
            'Фильтр Книг'   // Name
        );
        add_action( 'widgets_init', [$this, 'register_book_widget'] );
    }

    public function register_book_widget()
    {
        register_widget( __CLASS__ );
    }

    public  function widget($args, $instance)
    {
        //
        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . "<h1> {$instance['title']} </h1>" . $args['after_title'];
        }

        $bookPageId = get_option('book-page');

        if(isset($instance['taxonomies'])) {
            ?>
            <form name="myform"
                  method="get"
                  action="<?php echo isset( $bookPageId ) ? esc_url(get_page_link( $bookPageId)) : ""; ?>"
                  autocomplete="off">

                <?php
                if (get_option('permalink_structure') == ""):
                    ?>
                    <input type="hidden" name="page_id" value="<?php echo get_option('book-page'); ?>">
                    <?php
                endif;
                ?>

            <?php

            foreach ($instance['taxonomies'] as $taxName => $value) :
                $taxObj = get_taxonomy($taxName);
                if(isset($_GET[$taxName])) {
                    $termIds = filter_var_array($_GET[$taxName], FILTER_VALIDATE_INT);
                } else {
                    $termIds = [ false ];
                }
                ?>
                <h2> <?php echo  esc_html($taxObj->label); ?></h2>
                    <?php
                        $terms = get_terms($taxName);
                        foreach ($terms as $term) :
                            ?>
                                    <input type="checkbox"
                                           class="checkbox"
                                           id="<?php echo esc_attr($taxName.'-'.$term->term_id) ; ?>"
                                           name="<?php echo esc_attr($taxName)."[]"?>" value="<?php echo esc_attr($term->term_id) ?>"

                                            <?php
                                            echo checked (in_array($term->term_id, $termIds) ? 1 : 0) ;
                                            ?>
                                    />
                                    <label for="<?php echo esc_attr($taxName.'-'.$term->term_id) ; ?>"><?php _e( esc_html($term->name) ); ?></label><br />
                            <?php

                        endforeach;
                    ?>
                <?php
            endforeach;
            ?>
                <br>
            <input id="filter_submit_id"  type="submit" class="button button-primary" value="Фильтровать"/>
            </form>
            <?php
        }

        echo $args['after_widget'];
    }

    public function form( $instance )
    {
        // outputs the options form in the admin
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = "";
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label>
            <input class="widefat"
                   id="<?php echo $this->get_field_id( 'title' ); ?>"
                   name="<?php echo $this->get_field_name( 'title' ); ?>"
                   type="text" value="<?php echo esc_attr( $title ); ?>" />
        </p>
        <?php
        $taxonomies = get_object_taxonomies('book');
        foreach ($taxonomies as $taxonomy) :
            $taxObj = get_taxonomy($taxonomy);
            ?>
            <input type="checkbox" class="checkbox" id="<?php echo $this->get_field_id($taxonomy); ?>"
                   name="<?php echo $this->get_field_name($taxonomy); ?>"
                   value="1"

                  <?php echo  checked ((isset($instance['taxonomies'][$taxonomy])) ? $instance['taxonomies'][$taxonomy] : 0); ?>
            />
            <label for="<?php echo $this->get_field_id($taxonomy); ?>"><?php _e( "Фильтрация по \"".$taxObj->label."\"" ); ?></label><br />
            <?php
        endforeach;
    }

    public function update( $new_instance, $old_instance ) {
        // processes widget options to be saved
        $instance = [];

        $taxonomies = get_object_taxonomies('book');
        foreach ($taxonomies as $taxonomy) {
            $instance['taxonomies'][$taxonomy] =  !empty( $new_instance[$taxonomy] ) ? 1 : 0;
        }

           $instance['title'] = ( !empty( $new_instance['title'] ) ) ? sanitize_text_field( $new_instance['title'] ) : '';
        return $instance;
    }
}
