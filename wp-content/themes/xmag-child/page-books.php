<?php
/**
 * The template for displaying books posts.
 *
 * Template Name: Страница книг
 * @package xMag
 * @since xMag 1.0
 */

get_header(); ?>

<?php if ( have_posts() ) : ?>
	
	<?php if ( get_theme_mod('xmag_page_featured_image') && get_theme_mod('xmag_page_featured_image_size') == 'fullwidth' && has_post_thumbnail() ) : ?>
		
		<div class="featured-image">
			<header class="page-header">
                <?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
			</header>
			<div class="cover-bg" style="background-image:url(<?php the_post_thumbnail_url('xmag-thumb'); ?>)"></div>
		</div><!-- .featured-image -->
		
	<?php endif; ?>

	<div id="primary" class="content-area">

        <header class="page-header">
            <?php the_title( '<h1 class="page-title">', '</h1>' ); ?>
        </header>

			<?php
                    $paged = get_query_var('paged') ? intval(get_query_var('paged')) : 1;

                    $queryArr = filter_var_array($_GET, FILTER_VALIDATE_INT);

                    $args = [
                        'post_type'         => 'book',
                        'tax_query'         => \XmagChild\BookWPQuery::formTaxQuery( $queryArr ),
                        'paged'             => $paged
                    ];

                     include( get_stylesheet_directory(). '/template-parts/main-block.php');
            ?>

	</div><!-- #primary -->

<?php endif; ?>

<?php
wp_reset_query();
$option = get_option('book-page');
    if($option) {
        if (is_page($option)){
            get_sidebar('book-sidebar');
        }
        else {
            get_sidebar();
        }
    } else {
        get_sidebar();
    }
?>

<?php get_footer(); ?>
